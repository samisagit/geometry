# Install

`go get gitlab.com/samisagit/geometry/cmd/geometry`

# Usage

Basic useage:

`geometry -i inputfile.yaml -o outputfile.go -p package_name`

Package name will default to `procs`, the other two arguments are required. The command will generate a process definition, including the faces specified, and will generate the edges and spans to work through the process.

For e.g.

```yaml
processes:
  - name: change_password
    faces:
      - name: submit_email
        initial_face: true
        destinations:
          email_submitted: submit_token
      - name: submit_token
        destinations:
          token_submitted: change_password
      - name: change_password
```

would create a process in which the path would be:

`submit_email` --> `submit_token` --> `change_password`

given the process is fed the actions `email_submitted` and `token_submitted`.

Other actions will return a `NothingToDo` error.

The generated library exposes `PreHandler` and `PostHandler` which are of the signature `func(string) error`. `PreHandler` is called when a face is entered, `PostHandler` is called when the face is about to be exitted.
