package procs

import (
	"testing"
	"errors"
)

func TestLinearProgression(t *testing.T) {
	proc, err := StartLinearProcess("testSubject")
	if err != nil {
		t.Error(err)
	}

	if proc.Cursor().name != "a" {
		t.Errorf("expected initial face to be 'a', got %s", proc.Cursor().name)
	}

	err = proc.Progress(ActionLinearAAToB)
	if err != nil {
		t.Error(err)
	}

	if proc.Cursor().name != "b" {
		t.Errorf("expected face to be 'b', got %s", proc.Cursor().name)
	}

	err = proc.Progress(ActionLinearBBToC)
	if err != nil {
		t.Error(err)
	}

	if proc.Cursor().name != "c" {
		t.Errorf("expected face to be 'c', got %s", proc.Cursor().name)
	}
}

func TestProgressionFailsOnInvalidAction(t *testing.T) {
	proc, err := StartLinearProcess("testSubject")
	if err != nil {
		t.Error(err)
	}

	if proc.Cursor().name != "a" {
		t.Errorf("expected initial face to be 'a', got %s", proc.Cursor().name)
	}

	err = proc.Progress(ActionLinearBBToC) // incorrect action
	if !errors.Is(err, NothingToDoError) {
		t.Errorf("expected NothingToDoError, got %v", err)
	}
}

type transitionHarness struct {
	pre string
	post string
}

func (th *transitionHarness) handlerFactory(t *testing.T) func(name, subject string, state FaceState) error {
	return func(name, subject string, state FaceState) error {
		switch state {
		case Started:
			t.Logf("entering face %s\n", name)
			th.pre = name
		case Finished:
			t.Logf("exiting face %s\n", name)
			th.post = name
		}

		return nil
	}
}

func TestPreAndPostHandlersAreCalled(t *testing.T) {
	th := transitionHarness{}
	TransitionHandler = th.handlerFactory(t)

	proc, err := StartLinearProcess("testSubject")
	if err != nil {
		t.Error(err)
	}

	if th.pre != "a" {
		t.Errorf("expected pre data to be a, got %s", th.pre)
	}

	if th.post!= "" {
		t.Errorf("expected post data to be empty, got %s", th.post)
	}

	err = proc.Progress(ActionLinearAAToB)
	if err != nil {
		t.Error(err)
	}

	if th.pre != "b" {
		t.Errorf("expected pre data to be b, got %s", th.pre)
	}

	if th.post != "a" {
		t.Errorf("expected post data to be a, got %s", th.post)
	}
}
