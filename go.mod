module gitlab.com/samisagit/geometry

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	gopkg.in/yaml.v2 v2.3.0
)
