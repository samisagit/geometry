generate: testoutput/*.yaml templates/deps.go templates/process.go
	cd testoutput; go generate
test:
	go test ./... -v

