package templates

const Process = `package {{ .PackageName }}

import "fmt"

{{ range $proc := .Processes }}

	{{ range $face := .Faces }}

		// Declare edges
		{{ range $edgei, $edge := .Edges -}}
			var {{ ToCamel "edge_" $proc.Name "_" $face.Name "_" $edgei }} = edge{
				name: "{{ .Name }}",
			}
		{{ end }}

		// Declare spans
		{{ range $action, $span := .Spans }}
			var {{ ToCamel "span_"  $proc.Name "_" $action }} = span{
				{{ range $innerface := $proc.Faces -}}
					{{ range $edgei, $edge := $innerface.Edges -}}
						{{ if eq $span.Origin.Name .Name -}}
							origin: {{ ToCamel "edge_" $proc.Name "_" $innerface.Name "_" $edgei }},
						{{ end -}}
						{{ if eq $span.Insertion.Name .Name -}}
							insertion: {{ ToCamel "edge_" $proc.Name "_" $innerface.Name "_" $edgei }},
						{{ end -}}
					{{ end -}}
				{{ end -}}
			}

		{{ end }}

		// Declare faces
		var {{ ToCamel "face_" $proc.Name "_" $face.Name }} = face{
			name: "{{ ToCamel $face.Name }}",
			edges: []edge{
				{{ range $edgei, $edge := .Edges -}}
					{{ ToCamel "edge_" $proc.Name "_" $face.Name "_" $edgei }},
				{{ end }}
			},
			spans: map[string]span{
				{{ range $action, $span := .Spans -}}
					"Action{{ ToPascal $proc.Name "_" $face.Name "_" $action }}": {{ ToCamel "span_"  $proc.Name "_" $action }},
				{{ end -}}
			},
		}
	{{ end }}

	type {{ ToCamel .Name }}Action string

	const (
		{{ range $face := .Faces -}}
			{{ range $key, $span := $face.Spans -}}
				Action{{ ToPascal $proc.Name "_" $face.Name "_" $key }} {{ ToCamel $proc.Name }}Action = "Action{{ ToPascal $proc.Name "_" $face.Name "_" $key }}"
			{{ end -}}
		{{ end -}}
	)

	type {{ ToPascal .Name }}Process struct {
		Name string
		faces []*face
		Path []string
		Subject string
		Handler func(string, string, FaceState) error
	}

	func (p *{{ ToPascal .Name }}Process) Progress(action {{ ToCamel .Name}}Action) error {
		c := p.Cursor()

		span, ok := c.spans[string(action)]
		if !ok {
			return fmt.Errorf("action not recognised: %w", NothingToDoError)
		}

		destination := span.insertion.name

		for _, face := range p.faces {
			for _, edge := range face.edges {
				if edge.name == destination {
					p.Path = append(p.Path, face.name)
					p.Handler(c.name, p.Subject, Finished)
					c = p.Cursor()
					p.Handler(c.name, p.Subject, Started)

					return nil
				}
			}
		}

		panic("destination not found")
	}

	func (p *{{ ToPascal .Name }}Process) Cursor() *face {
		faceName := p.Path[len(p.Path) -1]

		for _, face := range p.faces {
			if face.name == faceName {
				return face
			}
		}

		panic("last path item not in faces")
	}

	func Start{{ ToPascal .Name }}Process(subject string) (*{{ ToPascal .Name }}Process, error) {
		p := &{{ ToPascal .Name }}Process {
			Name: "{{ .Name }}",
			Handler: TransitionHandler,	
			Subject: subject,
			faces: []*face{
				{{ range $face := .Faces -}}
					&{{ ToCamel "face_" $proc.Name "_" $face.Name }},
				{{ end -}}
			},
			Path: []string{
				{{ range $face := .Faces -}}
					{{ if eq $face.InitialFace true -}}
						"{{ $face.Name }}",
					{{ end -}}			
				{{ end -}}
			},
		}

		err := p.Handler(
			{{ range $face := .Faces -}}
				{{ if eq $face.InitialFace true -}}
					"{{ $face.Name }}",
				{{ end -}}			
			{{ end -}}
			p.Subject,
			Started,
		)

		if err != nil {
			return nil, err
		}

		return p, nil
	}

{{ end }}`
