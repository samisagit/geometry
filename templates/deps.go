package templates

const Deps = `package {{ .PackageName }}

import "errors"

var NothingToDoError = errors.New("cannot progress process: nothing to do")

type face struct {
       name string
       edges []edge
       spans map[string]span
}

type FaceState int

const (
	Started FaceState = iota
	Finished FaceState = iota
)

type span struct {
       origin edge
       insertion edge
}

type edgeName string

type edge struct {
       name edgeName
}

var TransitionHandler = func(name string, subject string, state FaceState) error {
	return nil
}
`
