package main

import (
	"io/ioutil"
	"text/template"
	"bytes"
	"go/format"
	"path/filepath"
	"flag"
	"fmt"

	"gitlab.com/samisagit/geometry/templates"
	"gitlab.com/samisagit/geometry/process"

	"gopkg.in/yaml.v2"
	"github.com/iancoleman/strcase"
)

type Input struct {
	Processes []struct{
		Name string `yaml:"name"`
		Faces []struct{
			Name string `yaml:"name"`
			Destinations map[string]string `yaml:"destinations"`
			InitialFace bool `yaml:"initial_face"`
		} `yaml:"faces"`
	} `yaml:"processes"`
}

type templateData struct {
	PackageName string
	Processes []*process.Process
}

func main() {
	inputPtr := flag.String("i", "", "yaml formatted process input")
	outputPtr := flag.String("o", "", "destination for go source output")
	packagePtr := flag.String("p", "procs", "package name for processes")
	flag.Parse()

	if *outputPtr == "" || *inputPtr == "" {
		panic("io args not present")
	}

	data, err := ioutil.ReadFile(*inputPtr)
	var t Input
	err = yaml.Unmarshal([]byte(data), &t)
	if err != nil {
		panic(err)
	}


	td := templateData{
		PackageName: *packagePtr,
		Processes: processToDomain(t),
	}

	tmpl := template.Must(template.New("procs").Funcs(
		template.FuncMap{
			"ToPascal": func(s ...interface{}) string {
				return strcase.ToCamel(fmt.Sprint(s...))
			},
			"ToCamel":func(s ...interface{}) string {
				return strcase.ToLowerCamel(fmt.Sprint(s...))
			},
			"inc": func(i int) int {
				return i + 1
			},
		},
	).Parse(templates.Process))

	if err := fileFromTemplate(tmpl, *outputPtr, td); err != nil {
		panic(err)
	}

	tmpl = template.Must(template.New("deps").Parse(templates.Deps))

	dir := filepath.Dir(*outputPtr)

	if err := fileFromTemplate(tmpl, dir + "/deps.gen.go", td); err != nil {
		panic(err)
	}
}

func processToDomain(input Input) []*process.Process {
	procs := make([]*process.Process, 0)
	for _, proc := range input.Processes {
		faces := make(map[string]*process.Face)

		for _, face := range proc.Faces {
			faces[face.Name] = process.NewFace(face.Name, face.InitialFace)
		}

		faceSlice := make([]*process.Face, 0)
		for _, face := range proc.Faces {
			f := faces[face.Name]

			for progression, relatedFace := range face.Destinations {
				fmt.Printf("spanning %s to %s with %s\n", f.Name, faces[relatedFace].Name, progression)
				process.SpanFaces(progression, f, faces[relatedFace])
			}

			faceSlice = append(faceSlice, f)
		}

		if !faceSlice[0].InitialFace {
			panic(fmt.Errorf("process %s has no entrypoint", proc.Name))
		}

		procs = append(procs, process.NewProcess(proc.Name, faceSlice[0], faceSlice[1:]...))
	}

	return procs
}

func fileFromTemplate(tmpl *template.Template, path string, td interface{}) error {
	var s string

	buf := bytes.NewBufferString(s)

	err := tmpl.Execute(buf, td)
	if err != nil {
		return fmt.Errorf("execution error : %w", err)
	}
	output := buf.Bytes()

	content, err := format.Source(output)
	if err != nil {
		fmt.Printf("warn: format error in %s:  %v", path, err)
		return ioutil.WriteFile(path, output, 0644)
	}

	return ioutil.WriteFile(path, content, 0644)
}
