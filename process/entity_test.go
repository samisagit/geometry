package process

import (
	"testing"
	"reflect"
)

func TestNewProcess(t *testing.T) {
	type newProcessArgs struct {
		name string
		// WARN: this only works with 2 faces
		faces []*Face
	}
	cases := []struct{
		name string
		args newProcessArgs
		want Process
	}{
		{
			name: "happy path",
			args: newProcessArgs{
				name: "test process",
				faces: []*Face{
					{Name: "First Face", InitialFace: true, Spans: make(map[string]span), Edges: make([]edge, 0)},
					{Name: "Second Face", Spans: make(map[string]span), Edges: make([]edge, 0)},
				},
			},
			want: Process{
				Name: "test process",
				Faces: []*Face{
					{Name: "First Face", InitialFace: true, Spans: make(map[string]span), Edges: make([]edge, 0)},
					{Name: "Second Face", Spans: make(map[string]span), Edges: make([]edge, 0)},
					{Name: "Second Face Exit", Spans: make(map[string]span), Edges: make([]edge, 0)},
				},
			},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(it *testing.T) {
			SpanFaces("test", c.args.faces[0], c.args.faces[1])

			got := NewProcess(c.args.name, c.args.faces[0], c.args.faces[1:]...)

			for _, gotFace := range got.Faces {
				foundFace := false
				for _, wantFace := range c.want.Faces {
					if gotFace.Name == wantFace.Name {
						foundFace = true
						break
					}
				}

				if !foundFace {
					t.Errorf("got unexpected face %s", gotFace.Name)
				}
			}


			for _, wantFace := range c.want.Faces {
				foundFace := false
				for _, gotFace := range got.Faces {
					if gotFace.Name == wantFace.Name {
						foundFace = true
						break
					}
				}

				if !foundFace {
					t.Errorf("face %s not found", wantFace.Name)
				}
			}
		})
	}
}

func TestNewFace(t *testing.T) {
	type newFaceArgs struct {
		name string
		initial bool
	}

	cases := []struct{
		name string
		args newFaceArgs
		want Face
	}{
		{
			name: "happy path",
			args: newFaceArgs{
				name: "test face",
				initial: true,
			},
			want: Face{
				Name: "test face",
				InitialFace: true,
				Edges: make([]edge, 0),
				Spans: make(map[string]span),
			},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(it *testing.T) {
			got := NewFace(c.args.name, c.args.initial)

			if !reflect.DeepEqual(*got, c.want) {
				it.Errorf("expected:\n%+v\ngot\n%+v", c.want, got)
			}
		})
	}
}

func TestSpanFaces(t *testing.T) {
	type spanFacesArgs struct {
		action string
		pre *Face
		post *Face
	}

	cases := []struct{
		name string
		args spanFacesArgs
	}{
		{
			name: "happy path",
			args: spanFacesArgs{
				action: "testAction",
				pre: &Face{
					Edges: make([]edge, 0),
					Spans: make(map[string]span),
				},
				post: &Face{
					Edges: make([]edge, 0),
					Spans: make(map[string]span),
				},
			},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(it *testing.T) {
			SpanFaces(c.args.action, c.args.pre, c.args.post)

			preFacesLength := len(c.args.pre.Edges)
			if preFacesLength != 1 {
				it.Errorf("expected pre to have 1 edge, got %d", preFacesLength)
			}

			postFacesLength := len(c.args.post.Edges)
			if postFacesLength != 1 {
				it.Errorf("expected pre to have 1 edge, got %d", postFacesLength)
			}

			if !reflect.DeepEqual(
				c.args.pre.Spans[c.args.action],
				span{Origin: &c.args.pre.Edges[0], Insertion: &c.args.post.Edges[0]},
			) {
				it.Errorf(
					"expected pre faces spans to include a span containing pre and post at key %s, got\n%+v",
					c.args.action,
					c.args.pre.Spans[c.args.action],
				)
			}
		})
	}
}
