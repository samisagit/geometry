package process

import (
	"fmt"

	"github.com/google/uuid"
)

type Process struct {
	Name string
	Faces []*Face
}

func NewProcess(name string, initialFace *Face, rest ...*Face) *Process {
	proc := &Process{
		Name: name,
		Faces: []*Face{initialFace},
	}

	proc.Faces = append(proc.Faces, rest...)

	addEndFaces(proc)

	return proc
}

func addEndFaces(p *Process) {
	for _, face := range p.Faces {
		hasExitPoint := false
		for _, span := range face.Spans {
			if span.Insertion != nil {
				hasExitPoint = true
				break
			}
		}

		if hasExitPoint {
			continue
		}

		endFaceName := fmt.Sprintf("%s%s", face.Name, "_termination")
		endFace := NewFace(endFaceName, false)
		action := fmt.Sprintf("%s_to_%s", face.Name, endFaceName)

		fmt.Printf("spanning %s to %s with %s\n", face.Name, endFaceName, action)
		SpanFaces(action, face, endFace)

		p.Faces = append(p.Faces, endFace)
	}
}

type Face struct {
	Name string
	Edges []edge
	Spans map[string]span
	InitialFace bool
}

func NewFace(name string, initial bool) *Face {
	return &Face{
		Name: name,
		Edges: make([]edge, 0),
		Spans: make(map[string]span),
		InitialFace: initial,
	}
}

func SpanFaces(action string, pre, post *Face) {
	preEdge := edge{
		Name: edgeName(uuid.New().String()),
	}
	postEdge := edge{
		Name: edgeName(uuid.New().String()),
	}

	s := span{
		Origin: &preEdge,
		Insertion: &postEdge,
	}

	pre.Edges = append(pre.Edges, preEdge)
	pre.Spans[action] = s
	post.Edges = append(post.Edges, postEdge)
}

type span struct {
	Origin *edge
	Insertion *edge
}

type edgeName string
type edge struct {
	Name edgeName
}
